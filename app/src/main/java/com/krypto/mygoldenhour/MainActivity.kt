package com.krypto.mygoldenhour

import android.Manifest
import android.content.IntentSender
import android.graphics.Color
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.GoogleMap
import com.google.android.libraries.maps.OnMapReadyCallback
import com.google.android.libraries.maps.SupportMapFragment
import com.google.android.libraries.maps.model.LatLng
import com.google.android.libraries.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.intentfilter.androidpermissions.PermissionManager
import com.krypto.mygoldenhour.viewmodels.MainViewModel
import com.krypto.mygoldenhour.viewstates.MapViewState
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.gms.common.api.Status
import com.google.android.libraries.maps.model.PolylineOptions
import com.google.android.libraries.places.api.net.PlacesClient
import com.krypto.mygoldenhour.databinding.PhaseRowItemBinding
import com.krypto.mygoldenhour.dialogs.PinHistoryDialogFragment
import com.krypto.mygoldenhour.utility.GHUtils
import com.krypto.mygoldenhour.viewmodels.models.HourData
import com.minimize.android.rxrecycleradapter.RxDataSource
import kotlinx.android.synthetic.main.date_control_layout.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(), OnMapReadyCallback, LifecycleOwner {


    private lateinit var viewModel: MainViewModel
    private lateinit var mGoogleMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var placesClient: PlacesClient

    companion object {
        const val LOCATION_SETTINGS_REQUEST = 2
        const val TAG = "main_activity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar_support)
        initViews()
        setupObservers()
    }

    override fun onStart() {
        super.onStart()
        verifyPermission()
    }

    private fun initViews() {
        // Initialize the AutocompleteSupportFragment.
        val autocompleteFragment =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment?

// Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, "AIzaSyCI04tvgJyOVRT5hoqa4yF9HJM8p0rdDMs")
        }

        viewModel = ViewModelProviders.of(this)[MainViewModel::class.java]
        val mapFragment: SupportMapFragment? =
            supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


        autocompleteFragment!!.setPlaceFields(
            Arrays.asList(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG
            )
        )

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                if (place.latLng != null) {
                    viewModel.updatePinLocation(place.latLng!!)
                }
            }

            override fun onError(status: Status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: $status")
            }
        })
    }

    private fun setupObservers() {
        viewModel.mainState.observe(::getLifecycle, ::updateUI)
        viewModel.currentDate.observe(::getLifecycle, ::updateDate)
        forward_date.setOnClickListener(viewModel)
        backward_date.setOnClickListener(viewModel)
        reset_date.setOnClickListener(viewModel)
        save_pin.setOnClickListener(viewModel)
        load_pin.setOnClickListener(viewModel)
    }

    private fun verifyPermission() {
        val permissionManager = PermissionManager.getInstance(applicationContext)
        permissionManager.checkPermissions(
            Collections.singleton(Manifest.permission.ACCESS_COARSE_LOCATION),
            object : PermissionManager.PermissionRequestListener {
                override fun onPermissionGranted() {

                    fusedLocationClient.lastLocation
                        .addOnSuccessListener { location: Location? ->
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                viewModel.updatePinLocation(
                                    LatLng(
                                        location.latitude,
                                        location.longitude
                                    )
                                )
                            }
                        }
                }

                override fun onPermissionDenied() {
                    askPermissionDialog()
                }
            })
    }


    private fun askPermissionDialog() {
        val mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval((10 * 1000).toLong())
            .setFastestInterval((1 * 1000).toLong())

        val settingsBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        settingsBuilder.setAlwaysShow(true)

        val result = LocationServices.getSettingsClient(this)
            .checkLocationSettings(settingsBuilder.build())

        result.addOnCompleteListener { task ->
            try {
                val response = task.getResult(ApiException::class.java)
            } catch (ex: ApiException) {
                when (ex.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        val resolvableApiException = ex as ResolvableApiException
                        resolvableApiException
                            .startResolutionForResult(
                                this@MainActivity,
                                LOCATION_SETTINGS_REQUEST
                            )
                    } catch (e: IntentSender.SendIntentException) {

                    }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just move the camera to Sydney and add a marker in Sydney.
     */
    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap ?: return
        mGoogleMap = googleMap
    }

    private fun setPinOnMap() {
        with(mGoogleMap) {
            clear()
            moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    viewModel.mPinLocation,
                    viewModel.ZOOM_LEVEL
                )
            )
            addMarker(MarkerOptions().position(viewModel.mPinLocation))
        }
    }

    private fun updateDate(date: Calendar) {
        date_textbox.setText(date.time.toString())
    }

    private fun updateUI(mapViewState: MapViewState) {
        when (mapViewState) {
            is MapViewState.SunriseHour -> {
                val sunPos =  GHUtils.findDest(viewModel.mPinLocation,mapViewState.startAngle,10.0)
                drawLine(sunPos)
            }
            is MapViewState.SunsetHour -> {
                val sunPos = GHUtils.findDest( viewModel.mPinLocation,mapViewState.startAngle,10.0)
                drawLine(sunPos)

            }
            is MapViewState.OnPinChange -> {
                setPinOnMap()
            }
            is MapViewState.PhaseList -> {
                val adapter = RxDataSource<PhaseRowItemBinding, HourData>(
                    R.layout.phase_row_item,
                    mapViewState.phases
                )
                adapter.bindRecyclerView(phase_list)
                adapter.asObservable().subscribe { holder ->
                    val binding = holder.viewDataBinding ?: return@subscribe
                    val data = holder.item
                    binding.descriptionTextbox.setText(data?.desc)
                    binding.time.setText(data?.time.toString())
                    binding.logo.background = getDrawable(data!!.idRes)
                }
            }
            is MapViewState.SavedPin ->{
                showDialog(mapViewState.savedPins)
            }
        }
    }


    private fun drawLine(sunPos :LatLng){
        mGoogleMap.addPolyline(
            PolylineOptions()
                .add(viewModel.mPinLocation, sunPos)
                .width(5f)
                .color(Color.RED)
        )
    }


    private fun showDialog(pinList : ArrayList<LatLng>){
        PinHistoryDialogFragment(pinList)
            .setDialogEventListener(object : PinHistoryDialogFragment.onDialogClickEvent {
                override fun onPositiveClick(selectedIndex: Int) {
                    viewModel.updatePinLocation(pinList[selectedIndex])
                }

            })
            .show(supportFragmentManager, "")

    }
}
