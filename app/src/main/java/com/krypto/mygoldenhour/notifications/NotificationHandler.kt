package com.krypto.mygoldenhour.notifications

import android.content.Context
import androidx.work.*
import java.util.concurrent.TimeUnit
import android.app.NotificationManager
import android.app.NotificationChannel
import android.app.PendingIntent
import android.content.Intent
import androidx.core.app.NotificationCompat
import com.krypto.mygoldenhour.MainActivity
import com.krypto.mygoldenhour.R
import java.util.*


class NotificationHandler : Worker{

    constructor(context: Context,workerParameters: WorkerParameters):super(context,workerParameters)

    companion object{
        val EXTRA_TITLE = "title"
        val EXTRA_TEXT = "text"
        val EXTRA_ID = "id"

        val TITLE = "Golden Hour"
        val TEXT = "Kit up to capture the best moments"

        fun scheduleReminder(duration: Long, data: Data, tag: String) {
            val notificationWork = OneTimeWorkRequest.Builder(NotificationHandler::class.java)
                .setInitialDelay(duration, TimeUnit.MILLISECONDS).addTag(tag)
                .setInputData(data).build()

            val instance = WorkManager.getInstance()
            instance.enqueue(notificationWork)
        }

    }

    override fun doWork(): Result {
        val title = inputData.getString(EXTRA_TITLE)
        val text = inputData.getString(EXTRA_TEXT)
        val id = inputData.getDouble(EXTRA_ID, 0.0).toInt()

        sendNotification(title!!, text!!, id)
        return Result.success()
    }



    private fun sendNotification(title: String, text: String, id: Int) {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(EXTRA_ID, id)

        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, 0)

        val notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT)
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(applicationContext, "default")
            .setContentTitle(title)
            .setContentText(text)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setAutoCancel(true)

        Objects.requireNonNull(notificationManager).notify(id, notification.build())
    }

}