package com.krypto.mygoldenhour.utility

import android.util.Log
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import com.google.android.libraries.maps.model.LatLng
import java.text.DecimalFormat
import kotlin.math.asin
import kotlin.math.cos
import kotlin.math.sin


object GHUtils {

    fun showFragment(
        @IdRes res: Int, fragmentToShow: Fragment,
        fragmentManager: FragmentManager,
        addToBackStack: Boolean,
        tag: String
    ) {
        fragmentManager.commit(allowStateLoss = true, body = {
            if (addToBackStack && fragmentManager.findFragmentByTag(tag) == null) {
                add(res, fragmentToShow, tag)
                addToBackStack(null)
            } else {
                replace(res, fragmentToShow, tag)
            }
        })
    }

    fun getDestinationPoint(source: LatLng, brng: Double, dist: Double): LatLng? {
        var brng = brng
        var dist = dist
        dist = dist / 6371
        brng = Math.toRadians(brng)

        val lat1 = Math.toRadians(source.latitude)
        val lon1 = Math.toRadians(source.longitude)
        val lat2 = Math.asin(
            Math.sin(lat1) * Math.cos(dist) + Math.cos(lat1) * Math.sin(dist) * Math.cos(brng)
        )
        val lon2 = (lon1 + Math.atan2(
            Math.sin(brng) * Math.sin(dist) *
                    Math.cos(lat1),
            (Math.cos(dist) - (Math.sin(lat1) * Math.sin(lat2)))
        ))
        return if (java.lang.Double.isNaN(lat2) || java.lang.Double.isNaN(lon2)) {
            null
        } else LatLng(Math.toDegrees(lat2), Math.toDegrees(lon2))
    }

    fun findDest(source: LatLng,brng: Double,d : Double):LatLng{
        val R = 6371e3
        var brng = Math.toRadians(brng)
        val lat1 = source.latitude
        val lon1 = source.longitude

        var lat2 = asin( sin(lat1) * cos(d/R) +
                Math.cos(lat1)*Math.sin(d/R)*Math.cos(brng) )
        var lon2 = lon1 + Math.atan2(Math.sin(brng)* sin(d/R) * cos(lat1),
            Math.cos(d/R)-Math.sin(lat1)*Math.sin(lat2));
        return LatLng(lat2,lon2)
    }

}