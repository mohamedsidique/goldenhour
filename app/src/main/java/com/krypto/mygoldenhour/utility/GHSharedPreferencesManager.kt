package com.krypto.mygoldenhour.utility

import android.content.Context
import android.content.SharedPreferences
import com.google.android.libraries.maps.model.LatLng
import com.google.gson.Gson

class GHSharedPreferencesManager(val name : String = "sp",val context: Context)
{
    private val prefs: SharedPreferences
    init {
        prefs = context.getSharedPreferences(name,Context.MODE_PRIVATE)
    }

    companion object{
        const val PIN_PREFS= "pps"
    }


    /**
     * Gets a [String] saved in [SharedPreferences]
     *
     * @param key Key of the value
     * @return value as [String] | null if not present
     */
    fun getString(key: String): String? {
        return prefs.getString(key, null)
    }

    /**
     * Save or replace a String value
     *
     * @param key
     * @param value
     */
    fun setString(key: String, value: String) {

        val editor = prefs.edit()
        editor.putString(key, value)
        editor.commit()
    }


    fun getSavedPinLocations():ArrayList<LatLng>{
        val pinLocations = ArrayList<LatLng>()
        prefs.all.keys.forEach { pinLocations.add(Gson().fromJson(it,LatLng::class.java)) }
        return pinLocations
    }
}