package com.krypto.mygoldenhour.viewmodels.models

import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import java.util.*

data class HourData(val desc : String,val time : String,@DrawableRes val idRes: Int)