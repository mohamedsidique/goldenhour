package com.krypto.mygoldenhour.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.libraries.maps.model.LatLng
import com.krypto.mygoldenhour.viewstates.MapViewState
import java.util.*
import com.krypto.mygoldenhour.solarcalc.SunCalc
import com.krypto.mygoldenhour.solarcalc.model.SunPhase
import android.view.View
import com.krypto.mygoldenhour.viewmodels.models.HourData
import kotlin.collections.ArrayList
import java.text.SimpleDateFormat
import android.nfc.NfcAdapter.EXTRA_ID
import androidx.work.Data
import com.google.gson.Gson
import com.krypto.mygoldenhour.R
import com.krypto.mygoldenhour.notifications.NotificationHandler
import com.krypto.mygoldenhour.notifications.NotificationHandler.Companion.EXTRA_TEXT
import com.krypto.mygoldenhour.notifications.NotificationHandler.Companion.EXTRA_TITLE
import com.krypto.mygoldenhour.notifications.NotificationHandler.Companion.TEXT
import com.krypto.mygoldenhour.notifications.NotificationHandler.Companion.TITLE
import com.krypto.mygoldenhour.utility.GHSharedPreferencesManager


class MainViewModel : ViewModel(),View.OnClickListener{

    private lateinit var _mainState: MutableLiveData<MapViewState>

    var mPinLocation = LatLng(-33.862, 151.21)
    val currentDate : MutableLiveData<Calendar> = MutableLiveData(Calendar.getInstance())
    val ZOOM_LEVEL = 13f


    val mainState: LiveData<MapViewState>
        get() {
            if (!::_mainState.isInitialized) {
                _mainState = MutableLiveData()
//                findItemsInteractor.findItems(::onItemsLoaded)
            }
            return _mainState
        }


    public fun onLocationChange(latLng: LatLng,d : Calendar){

        // now
// Paris coordinates
        val LAT = latLng.latitude
        val LON = latLng.longitude
        // get a list of phases at a given location & day
        val sunPhases = SunCalc.getPhases(d, LAT, LON)
        val hoursList = ArrayList<HourData>()
        val dateFormatter = SimpleDateFormat("HH:mm:ss")
        for (phase in sunPhases) {
            val startTime = phase.startDate.time
            val endTime = phase.endDate.time
            val startAngle = phase.startAngle
            val endAngle = phase.endAngle

            var mapViewState : MapViewState? = null
            when(phase.name)
            {
                SunPhase.Name.SUNRISE ->{
                    mapViewState = MapViewState.SunriseHour(startTime,endTime,startAngle,endAngle)
                    hoursList.add(HourData( phase.name.toString(),dateFormatter.format(startTime), R.drawable.ic_sun_rise))
                }
                SunPhase.Name.SUNSET->{
                    mapViewState = MapViewState.SunsetHour(startTime,endTime,startAngle,endAngle)
                    hoursList.add(HourData(phase.name.toString(),dateFormatter.format(startTime), R.drawable.ic_sun_set))
                }
                SunPhase.Name.GOLDEN_HOUR_MORNING -> {
                    setUpRemainder(phase.startDate)
                    mapViewState = MapViewState.GoldenHour(startTime,endTime,startAngle,endAngle,false)
                    hoursList.add(HourData(phase.name.toString(),dateFormatter.format(startTime), R.drawable.ic_sun_rise))
                }
                SunPhase.Name.GOLDEN_HOUR_EVENING -> {
                    setUpRemainder(phase.endDate)
                    mapViewState = MapViewState.GoldenHour(startTime,endTime,startAngle,endAngle,true)
                    hoursList.add(HourData(phase.name.toString(),dateFormatter.format(startTime), R.drawable.ic_sun_set))
                }
            }
            if(mapViewState != null) {
                _mainState.value = mapViewState
            }
        }
        _mainState.value = MapViewState.PhaseList(phases = hoursList)

    }

    fun updatePinLocation(latLng: LatLng) {
        mPinLocation = latLng
        _mainState.value = MapViewState.OnPinChange(latLng)
        onLocationChange(latLng,currentDate.value!!)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backward_date->{
                val currentDateValue = currentDate.value
                currentDateValue?.add(Calendar.DAY_OF_YEAR,-1)
                currentDate.value= currentDateValue
                onLocationChange(mPinLocation,currentDateValue!!)
            }
            R.id.forward_date->{
                val currentDateValue = currentDate.value
                currentDateValue?.add(Calendar.DAY_OF_YEAR,1)
                currentDate.value= currentDateValue
                onLocationChange(mPinLocation,currentDateValue!!)
            }
            R.id.reset_date->{
                currentDate.value= Calendar.getInstance()
                onLocationChange(mPinLocation,currentDate.value!!)
            }
            R.id.save_pin->{
                val prefs = GHSharedPreferencesManager(GHSharedPreferencesManager.PIN_PREFS,v.context)
                prefs.setString(Gson().toJson(mPinLocation),"1")
            }
            R.id.load_pin->{
                val prefs = GHSharedPreferencesManager(GHSharedPreferencesManager.PIN_PREFS,v.context)
                val pinList = prefs.getSavedPinLocations()
                _mainState.value = MapViewState.SavedPin(pinList)
            }
        }
    }


    private fun createWorkInputData(title: String, text: String, id: Double): Data {
        return Data.Builder()
            .putString(EXTRA_TITLE, title)
            .putString(EXTRA_TEXT, text)
            .putDouble(EXTRA_ID, id)
            .build()
    }

    private fun getAlertTime(userInput: Int): Long {
        val cal = Calendar.getInstance()
        cal.add(Calendar.MINUTE, userInput)
        return cal.timeInMillis
    }


    private fun setUpRemainder(date : Calendar){
        val minutesBeforeAlert = 30
        val alertTime = getAlertTime(minutesBeforeAlert) - date.time!!.time
        val data = createWorkInputData(TITLE, TEXT, mPinLocation.latitude+mPinLocation.longitude)
        NotificationHandler.scheduleReminder(alertTime, data, mPinLocation.toString())
    }
}