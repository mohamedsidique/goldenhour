package com.krypto.mygoldenhour.viewstates

import com.google.android.libraries.maps.model.LatLng
import com.krypto.mygoldenhour.viewmodels.models.HourData
import java.util.*

sealed class MapViewState {
    class SunsetHour(
        val sunsetTime: Date,
        val endTime: Date,
        val startAngle: Double,
        val endAngle: Double
    ) : MapViewState()

    class SunriseHour(
        val sunriseTime: Date,
        val endTime: Date,
        val startAngle: Double,
        val endAngle: Double
    ) : MapViewState()

    class GoldenHour(
        val sunriseTime: Date,
        val endTime: Date,
        val startAngle: Double,
        val endAngle: Double,
        val isEvening : Boolean
    ) : MapViewState()

    class MoonPosition(val moonriseTime: String) : MapViewState()

    class MoonsetHour(val moonsetTime: String) : MapViewState()

    class OnPinChange(val latLng: LatLng?) : MapViewState()

    class PhaseList(val phases: ArrayList<HourData>) : MapViewState()

    class SavedPin(val savedPins: ArrayList<LatLng>) : MapViewState()


}