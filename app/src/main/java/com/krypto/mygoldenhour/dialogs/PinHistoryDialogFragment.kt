package com.krypto.mygoldenhour.dialogs

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.google.android.libraries.maps.model.LatLng
import com.krypto.mygoldenhour.R
import com.krypto.mygoldenhour.databinding.PinListBinding
import com.krypto.mygoldenhour.databinding.PinListItemBinding
import com.minimize.android.rxrecycleradapter.RxDataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class PinHistoryDialogFragment(val pinHistoryList: ArrayList<LatLng>) : DialogFragment()
{
    private var _dialogEventListener: onDialogClickEvent? = null

    interface onDialogClickEvent {
        fun onPositiveClick(selectedIndex : Int)

    }
    fun setDialogEventListener(_clickEventListener: onDialogClickEvent):PinHistoryDialogFragment {
        _dialogEventListener = _clickEventListener
        return this
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = arguments?.getString("productTitle")

        isCancelable = false
        val builder: AlertDialog.Builder
        val customDialogView : PinListBinding = DataBindingUtil.inflate(activity!!.layoutInflater,
            R.layout.pin_list, null,false)



        customDialogView.cancelBtn.setOnClickListener {
            dismiss()
        }

        val dataSource = RxDataSource<PinListItemBinding,LatLng>(R.layout.pin_list_item,pinHistoryList)

        dataSource.asObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {holder ->
                val binding = holder.viewDataBinding!!
                val item = holder.item!!
                binding.pinCoord.text = item.toString()
                binding.root.setOnClickListener {
                    _dialogEventListener?.onPositiveClick(holder.adapterPosition)
                    dismiss()
                }
            }


        dataSource.bindRecyclerView(customDialogView.pinList)

        builder = AlertDialog.Builder(activity!!, R.style.DialogTheme)
            .setMessage(title)
            .setView(customDialogView.root)

        val dialog = builder.create()

        val v = dialog?.window?.getDecorView()
        v?.setBackgroundResource(android.R.color.transparent)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog?.window?.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER


        dialog?.window?.attributes = lp

        return dialog
    }

}